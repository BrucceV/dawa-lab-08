const express = require("express");
const mongoose = require('mongoose');
const app = express();
const bodyParser = require("body-parser");
const userRouter = require('./routes/usuario');
require('./config/config');

//const port = process.env.PORT || 3000;

app.use('/api', userRouter);

//create application/x-ww-form-urlencoded parser
app.use(bodyParser.urlencoded({extended:false}));

//create application/jaon parser
app.use(bodyParser.json());

mongoose.connect(
    process.env.URLDB,
    { useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true},
     (err, res) =>{
    if(err) throw err;

    console.log('Base marrana de datos Online')
});

app.listen(process.env.PORT, () =>
 console.log(`Escuchando tu puerto marrano ${process.env.PORT}`)
 );